import React from "react";
import UserList from "./users/list";
import CreateUser from "./users/create-user";

function App() {
    return (
        <div style={{padding: "2rem", maxWidth: "1200px", margin: "auto"}}>
            <UserList/>
            <CreateUser/>
        </div>
    );
}

export default App;
