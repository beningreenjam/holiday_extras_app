import React from "react";

export default function CreateUser() {
    const [data, setData] = React.useState({});
    const [errors, setErrors] = React.useState([]);
    const [submitting, setSubmitting] = React.useState(false);

    const handleSubmit = React.useCallback(async (event) => {
        event.preventDefault();
        setErrors([]);
        setSubmitting(true);
        const res = await fetch(
            "http://localhost:8000/user", {
                method: "POST",
                mode: "cors",
                body: JSON.stringify(data),
                headers: {
                    Origin: "localhost:3000",
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            }
        )
            .then(res => res.json())
            .catch(console.error)
            .finally(() => setSubmitting(false))

        const {status, message, data: _data} = res || {};
        if (status === 'success') {
            event.target.reset();
            setData({});
            return;
        }

        setErrors(_data || message || ["Oops, something went wrong."]);
    }, [data]);

    const handleValueChange = React.useCallback((event) => {
        const {name, value} = event.target;
        setData((_data) => ({..._data, [name]: value}));
    }, []);

    return (
        <form onSubmit={handleSubmit}>
            <h2>Add user</h2>
            {errors.length > 0 && (
                <div style={{backgroundColor: "#ff9a9a", color: "#f00", padding: "0.5rem 1rem", marginBottom: "1rem"}}>
                    {errors.map((error, index) => <p key={`${error}-${index}`}>{error}</p>)}
                </div>
            )}
            <input placeholder="Given name" name="givenName" onChange={handleValueChange}/>
            <input placeholder="Family name" name="familyName" onChange={handleValueChange}/>
            <input placeholder="Email" name="email" type="email" onChange={handleValueChange}/>
            <button type="submit" disabled={submitting}>Create New User</button>
        </form>
    )
}