import React from "react";
import useSWR from "swr";

export default function UserList() {
    const {data} = useSWR("http://localhost:8000/user", async (url) => {
        return fetch(url).then(res => res.json()).catch(console.log);
    }, {refreshInterval: 1000});

    const users = (data?.data ?? []);

    return (
        <div style={{textAlign: "left"}}>
            <h2>Users</h2>
            {users.length === 0 && <p>No users yet</p>}
            {users.map(({givenName, familyName, email}) => {
                return (
                    <div key={email} className="user">
                        <p><strong>Given name:</strong> {givenName}</p>
                        <p><strong>Family name:</strong> {familyName}</p>
                        <p><strong>Email address:</strong> {email}</p>
                        <hr/>
                    </div>
                )
            })}
        </div>
    )
}